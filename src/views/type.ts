/*
 * @Author: xiaosihan 
 * @Date: 2023-12-10 04:36:29 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-01-07 20:57:31
 */


// 模型数据
export type ModelData = {
    id: string | number,
    name: string,
    url: string,
    floorId: string | number,
    positionX: number,
    positionY: number,
    positionZ: number,
    heading: number,
    pitch: number,
    roll: number,
    info?: string
}


// 文本标签数据
export type LabelData = {
    id: string | number,  // id
    name: string, // 备用字段
    floorId: string | number,
    positionX: number,
    positionY: number,
    positionZ: number,
    heading: number,
    pitch: number,
    roll: number,
    icon: string, // 图标
    textContainer: string // 标签文本
}

// 监控的数据
export type MonitorData = {
    id: string | number,
    floorId: string | number,
    name: string,
    url: string,
    positionX: number,
    positionY: number,
    positionZ: number,
    heading: number,
    pitch: number,
    roll: number,
}

// 楼层模型数据
export type FloorData = {
    id: string | number,
    name: string,
    url: string,
    positionX: number,
    positionY: number,
    positionZ: number,
    heading: number,
    pitch: number,
    roll: number
}