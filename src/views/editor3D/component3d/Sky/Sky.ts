/*
 * @Author: xiaosihan 
 * @Date: 2022-05-20 18:03:58 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-12-05 13:28:58
 */

import { BufferGeometry, DoubleSide, Mesh, MeshBasicMaterial, SphereGeometry } from "three";

import threeLoader from "three-base/threeLoader";
import downloadPNG from "./download.png";

/**
 * 天空球
 * @export
 * @class Sky
 * @extends {Mesh}
 */
export default class Sky extends Mesh<BufferGeometry, MeshBasicMaterial> {

    constructor() {
        super(Sky.geometry, Sky.material);
    }

    static geometry = new SphereGeometry(500000, 100, 100);

    static material = new MeshBasicMaterial({
        side: DoubleSide,
        map: threeLoader.getTexture(downloadPNG)
    });

}