/*
 * @Author: xiaosihan 
 * @Date: 2023-12-10 17:02:43 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-01-07 18:36:57
 */

import utils from "@utils";
import { BoxGeometry, InstancedMesh, MeshBasicMaterial } from "three";


// 模型点击事件
export default class ModelEventBox extends InstancedMesh {

    constructor() {
        super(ModelEventBox.geometry, ModelEventBox.materil, 0);
    }

    static geometry = (() => {
        const geometry = new BoxGeometry(1, 1, 1, 1, 1, 1);
        geometry.translate(0, 0.5, 0);
        return geometry;
    })();

    static materil = new MeshBasicMaterial({
        alphaTest: 1.1,
        // alphaTest: utils.isDev ? 0.1 : 1.1,
        wireframe: true,
        color: "#ff0000"
    });
}

