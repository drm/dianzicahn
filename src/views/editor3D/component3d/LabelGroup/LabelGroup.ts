/*
 * @Author: xiaosihan 
 * @Date: 2023-06-12 00:56:08 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-06-12 01:15:37
 */


import { autorun, toJS } from "mobx";
import { Group } from "three";
import Label from "./Label";
import editor3DStore from "@views/editor3D/editor3DStore";

// 文本弹窗数组
export default class LabelGroup extends Group {
    constructor() {
        super();
    }

    declare children: Array<Label>;

    type = "labelGroup";

    // 时时通过数据更新标签的位置
    dispose = autorun(() => {
        const { state } = editor3DStore;
        const labelDatas = toJS(editor3DStore.labelDatas);
        const children = [...this.children];
        this.clear();

        // 遍历机柜对象和数据
        for (let i = 0; i < Math.max(children.length, labelDatas.length); i++) {

            const labelData = labelDatas[i];

            // 有机柜有数据 就修改机柜对象
            if (labelData) {
                const textModal = children[i] || new Label();
                textModal.setData(labelData);
                // 添加模型时,事件盒子关闭鼠标事件
                Object.assign(textModal.userData, { enableEvent: state === "" });
                this.add(textModal);
            }
        }
    });

}