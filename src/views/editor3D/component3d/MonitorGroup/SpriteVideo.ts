/*
 * @Author: xiaosihan 
 * @Date: 2023-12-23 03:45:58 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-12-23 10:22:17
 */

import { BufferAttribute, BufferGeometry, Euler, Matrix4, Points, Quaternion, ShaderChunk, ShaderMaterial, Sprite, SpriteMaterial, Vector3, VideoTexture } from "three";
import Hls from "hls.js";

// 视频弹窗
export default class SpriteVideo extends Points {
    constructor() {
        super();
    }

    geometry = (() => {
        const geometry = new BufferGeometry();
        let position = new BufferAttribute(new Float32Array([0, 0, 0]), 3, false);
        let size = new BufferAttribute(new Float32Array([10]), 1, false);
        geometry.setAttribute("position", position);
        geometry.setAttribute("size", size);
        return geometry;
    })();

    video = (() => {
        const video = document.createElement("video");
        video.muted = true;
        video.autoplay = true;
        video.style.position = "fixed";
        video.style.left = "0px";
        video.style.top = "0px";
        video.style.opacity = "0";
        video.style.pointerEvents = "none";
        document.body.appendChild(video);
        return video;
    })();



    videoTexture = new VideoTexture(this.video);

    material = new ShaderMaterial({
        uniforms: {
            resolution: { value: 1 }, // 贴图纹理
            map: { value: this.videoTexture }, // 贴图纹理
        },
        vertexShader: `
            ${ShaderChunk.common}
            ${ShaderChunk.logdepthbuf_pars_vertex}
            attribute float size;
            void main() {
                vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
				gl_PointSize = size * ( 812.0 / -mvPosition.z );
				gl_Position = projectionMatrix * mvPosition;
                ${ShaderChunk.logdepthbuf_vertex}
            }
        `,
        fragmentShader: `
            ${ShaderChunk.logdepthbuf_pars_fragment}
            uniform sampler2D map; // 贴图
            uniform float resolution; // 分辨率
            
            void main() {
                vec2 uv = vec2( (gl_PointCoord.x * 2.0 / resolution) - ((2.0 / resolution - 1.0) * 0.5) , 1.0 - ( 2.0 * gl_PointCoord.y) );
                gl_FragColor  = texture2D(map, uv);
                if(uv.x < 0.0 || uv.x > 1.0 || uv.y < 0.0 || uv.y > 1.0){
                    // gl_FragColor.a = 0.5;
                    discard;
                }
                ${ShaderChunk.logdepthbuf_fragment}
            }
        `,
        depthTest: true,
        depthWrite: true,
        transparent: true
    });

    hls = (() => {
        const hls = new Hls();

        hls.on(Hls.Events.MANIFEST_PARSED, () => {
            // this.video.play();
            // console.log("加载成功");
        });

        hls.on(Hls.Events.LEVEL_LOADED, () => {
            const { width, height } = hls.levels[0];
            this.material.uniforms.resolution.value = width / height;
        });

        // hls.on(Hls.Events.ERROR, (event, data) => {
        //     //   console.log(event, data);
        //     // 监听出错事件
        //     console.log("加载失败");
        // });

        // hls.loadSource('https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8');
        // hls.attachMedia(this.video);

        return hls;
    })();

    url = "";

    // 设置视频流连接
    setUrl(url: string) {
        if (this.url !== url) {
            this.url = url;
            this.hls.loadSource(url);
            this.hls.attachMedia(this.video);
        }
    }


}

