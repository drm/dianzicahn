/*
 * @Author: xiaosihan 
 * @Date: 2023-12-10 04:27:29 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-01-13 11:23:15
 */


import { FloorData, LabelData, ModelData, MonitorData } from "@views/type";
import { observable, configure, toJS } from "mobx";
import { generateUUID } from "three/src/math/MathUtils";
configure({ enforceActions: "never" });

const editor3DStore = window.editor3DStore = observable({

    //添加模型中
    state: "" as "" | "addModel" | "addLabel" | "addMonitor" | "addFloor",
    addModelUrl: "", // 模型的地址
    addModelInfo: "", // 模型的文本信息
    monitorUrl: "", // 需要添加的监控视频的地址
    addLabelId: "" as string | number,
    addFloorUrl: "", // 需要添加的楼层的id
    addFloorid: "", // 需要添加的楼层id 
    //添加模型
    addModel(url: string, info: string = "") {
        this.state = "addModel";
        this.addModelUrl = url;
        this.addModelInfo = info;
        this.activeFloorId = "";
    },
    // 添加标签
    addLabel(labelData: LabelData) {
        this.state = "addLabel";
        this.addLabelId = labelData.id;
        const labeldatas = this.getAlllabelDatas();
        labeldatas.push(labelData);
        this.setLabelDatas(labeldatas);
    },
    // 添加监控
    addMonitor(url = "") {
        this.state = "addMonitor";
        this.monitorUrl = url;
    },
    // 添加楼层模型
    addFloor(url = "") {
        if (url) {
            this.state = "addFloor";
            const floorId = generateUUID();
            this.addFloorid = floorId;
            this.activeFloorId = "";
            const floorDatas = this.getFloorDatas();
            floorDatas.push({
                id: floorId,
                name: "",
                url: url,
                positionX: Infinity,
                positionY: Infinity,
                positionZ: Infinity,
                heading: 0,
                pitch: 0,
                roll: 0
            });
            this.setFloorDatas(floorDatas);
        }
    },
    //取消添加
    cancelAdd() {
        this.state = "";
        this.addModelUrl = "";
        this.addLabelId = "";
        this.addFloorid = "";
    },

    //显示详情弹窗
    showInfoModal: false,
    infoModalPosition: { x: 0, y: 0, z: 0 },
    setInfoModal(show: boolean, infoModalPosition: { x: number, y: number, z: number } = { x: 0, y: 0, z: 0 }) {
        this.showInfoModal = show;
        this.infoModalPosition = infoModalPosition;
    },

    //模型数据
    modelDatas: [
        // {
        //     id: 1,
        //     name: "测试1",
        //     url: "/demo.glb",
        //     positionX: 0,
        //     positionY: 0,
        //     positionZ: 0,
        //     rotationY: 0,
        //     size: 5
        // },

    ] as Array<ModelData>,
    // 设置模型的数据
    setModelDatas(modelDatas: Array<ModelData>) {
        this.modelDatas = modelDatas;
    },
    //获取全部配置数据
    getModelDatas(): Array<ModelData> {
        return toJS(this.modelDatas);
    },
    // 根据id获取参数
    getModelDataById(id: string | number) {
        const modelData = this.modelDatas.find(d => d.id === id);
        return toJS(modelData);
    },
    //根据id修改参数配置
    setModelData(params: Partial<ModelData>) {
        const { id } = params;
        const modelData = this.modelDatas.find(d => d.id === id);
        if (modelData) {
            Object.assign(modelData, params);
        }
    },
    // 根据id删除配置数据
    deleteModelDate(id: string | number) {
        this.modelDatas = toJS(this.modelDatas).filter(d => d.id !== id);
    },

    //选中模型的id
    activeId: "" as string | number,
    setActiveId(id: string | number = "", offsetY = 0) {
        this.activeId = id;

        const modelData = this.getModelDataById(id);
        if (modelData) {
            editor3DStore.setInfoModal(true, {
                x: modelData.positionX,
                y: modelData.positionY + offsetY,
                z: modelData.positionZ,
            })
        } else {
            editor3DStore.setInfoModal(false);
        }

    },

    //标签数据
    labelDatas: [] as Array<LabelData>,
    setLabelDatas(labelDatas: Array<LabelData>) {
        this.labelDatas = labelDatas;
    },
    // 获取所有标签数据
    getAlllabelDatas() {
        return toJS(this.labelDatas);
    },
    setLabelDataById(id: string | number, labelDatas: Partial<LabelData>) {
        this.labelDatas.map(d => {
            if (d.id === id) {
                Object.assign(d, labelDatas);
            }
        })
    },
    getLabelDataById(id: string | number) {
        return toJS(this.labelDatas.find(d => d.id === id));
    },
    // 根据id删除配置数据
    deleteLabelDate(id: string | number) {
        this.labelDatas = toJS(this.labelDatas).filter(d => d.id !== id);
    },
    // 当前选中的标签
    lookLabelId: "",
    setLookLabelId(lookLabelId: string) {
        this.lookLabelId = lookLabelId;
    },

    //监控数据
    monitorDatas: [] as Array<MonitorData>,
    activeMonitorId: "" as string | number,
    setActiveMonitorId(id: string | number) {
        this.activeMonitorId = id;
    },
    //设置监控数据
    setMonitorDatas(monitorDatas: Array<MonitorData>) {
        this.monitorDatas = monitorDatas;
    },
    // 添加监控数据
    addMonitorData(monitorData: MonitorData) {
        this.monitorDatas.push(monitorData);
    },
    // 通过id修改监控设置
    setMonitorDataById(id: string | number, monitorData: Partial<MonitorData>) {
        this.monitorDatas.map(d => {
            if (d.id === id) {
                Object.assign(d, monitorData);
            }
        })
    },
    // 获取全部监控数据
    getMonitorDatas() {
        return toJS(this.monitorDatas);
    },
    // 通过id获取监控数据
    getMonitorDataById(id: string | number): MonitorData | undefined {
        const monitorDatas = this.getMonitorDatas();
        return monitorDatas.find(m => m.id === id);
    },
    // 通过id 删除监控数据
    deleteMonitorDataById(id: string | number) {
        this.monitorDatas = toJS(this.monitorDatas).filter(d => d.id !== id);
    },

    // 地面模型
    groundUrl: "",
    setGroundUrl(groundUrl: string) {
        this.groundUrl = groundUrl;
    },

    // 设置地面模型中
    groundLoading: false,
    setGroundLoading(groundLoading: boolean) {
        this.groundLoading = groundLoading;
    },

    //楼层数据
    floorDatas: [] as Array<FloorData>,
    // 设置楼层模型
    setFloorDatas(floorDatas: Array<FloorData>) {
        this.floorDatas = floorDatas;
    },
    //选中的楼层id
    activeFloorId: "" as string | number, // "" 空代表地面
    setActiveFloorId(activeFloorId: string | number = "") {
        this.activeFloorId = activeFloorId;
        this.setActiveId("");
    },
    //进入楼层的id 时取消选中
    inFloorId: "" as string | number, // "" 空代表地面
    setInFloorId(inFloorId: string | number = "") {
        this.inFloorId = inFloorId;
        this.activeFloorId = "";
    },
    //更新楼层数据
    setFloorDataById(floorId: string, floorData: Partial<FloorData>) {
        this.floorDatas.map(d => {
            if (d.id === floorId) {
                Object.assign(d, floorData);
            }
        })
    },
    // 通过id获取楼层数据
    getFloorDataById(id: string | number): FloorData | undefined {
        const floorDatas = this.getFloorDatas();
        return floorDatas.find(m => m.id === id);
    },
    //更具楼层id删除楼层数据
    deleteFloorById(floorId: string) {
        this.floorDatas = toJS(this.floorDatas).filter(d => d.id !== floorId);
    },
    //获取楼层数据
    getFloorDatas() {
        return toJS(this.floorDatas);
    }

});

export default editor3DStore;