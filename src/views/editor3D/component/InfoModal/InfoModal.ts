/*
 * @Author: xiaosihan 
 * @Date: 2024-01-13 11:07:37 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2024-01-13 11:55:43
 */
import { generateUUID } from "three/src/math/MathUtils";
import spriteModalPNG from "./spriteModal.png";
import editor3DStore from "@views/editor3D/editor3DStore";

export default class InfoModal {

    constructor() {

    }

    root = (() => {
        const root = document.createElement("div");
        Object.assign(root.style, {
            position: "absolute",
            display:"none",
            zIndex: 1,
            top: "0px",
            left: "0px",
            backgroundImage: `url(${spriteModalPNG})`,
            width: "330px",
            height: "260px",
            backgroundSize: "100% auto",
            padding: "40px 37px 46px 34px",
            boxSizing: "border-box"
        });
        return root;
    })();

    // 标题
    title = (() => {
        const title = document.createElement("div");
        Object.assign(title.style, {
            color: "#ffffff",
            fontSize: "20px",
            height: "28px",
            textAlign: "left"
        });
        this.root.appendChild(title);
        return title;
    })();

    //关闭视频按钮
    close = (() => {
        const close = document.createElement("span");
        close.innerText = "×";
        Object.assign(close.style, {
            display: "inline-block",
            border: "2px solid #ff0000",
            width: "20px",
            height: "20px",
            lineHeight: "20px",
            fontSize: "18px",
            fontWeight: "bold",
            borderRadius: "4px",
            color: "#ff0000",
            position: "absolute",
            right: "50px",
            top: "41px",
            cursor: "pointer",
            backgroundColor: "#f5cccc"
        });
        close.onclick = () => {
            editor3DStore.setActiveId("");
        }
        this.root.appendChild(close);
        return close;
    })();

    listClassName = `list_${generateUUID()}`;

    // 列表
    list = (() => {
        const list = document.createElement("div");
        list.className = this.listClassName;
        Object.assign(list.style, {
            width: "256px",
            height: "140px",
            overflowY: "auto",
            textAlign: "left",
            marginTop: "7px",
            color: "#4cbdff"
        });
        this.root.appendChild(list);
        return list;
    })();

    //样式
    style = (() => {
        const style = document.createElement("style");
        style.innerText = `
        .${this.listClassName}{
            overflow-y: auto;
            &::-webkit-scrollbar {
                width: 4px;
                height: 4px;
                background-color: rgba(0, 0, 0, 0);
            }
            &::-webkit-scrollbar-track {
                box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
                border-radius: 10px;
                background-color: rgba(0, 0, 0, 0);
            }
            &::-webkit-scrollbar-thumb {
                border-radius: 10px;
                box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
                background-color: #1890ff;
            }
        }
        `;
        this.root.appendChild(style);
        return style;
    })();

    // 设置标题
    setTitle(title: string) {
        this.title.innerText = title;
    }

    //设置内容
    setContent(content: string = "") {
        this.list.innerText = content;
    }

    //是否显示
    setShow(show: boolean) {
        Object.assign(this.root.style, {
            display: show ? "block" : "none"
        });
    }
    //设置位置
    setPosition(x: number, y: number) {
        const { clientHeight, clientWidth } = this.root;
        this.root.style.left = `${x - (clientWidth / 2)}px`;
        this.root.style.top = `${y - clientHeight - 10}px`;
    }

}