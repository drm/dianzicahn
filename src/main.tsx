/*
 * @Author: xiaosihan 
 * @Date: 2022-08-20 17:08:13 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2023-12-05 13:27:13
 */

import "./index.css";
import editor3D from "@views/editor3D/editor3D";
editor3D.init();
// editor3D.setContainer(document.getElementById('editor3d')! as HTMLDivElement);
