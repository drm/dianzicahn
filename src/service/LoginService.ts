import Service from './Service';

class LoginService extends Service {

    // 登录 邮箱作为用户名登录
    login(email: string, password: string) {
        return this._post('/login', { email, password });
    }

    // 获取用户名片夹里名片列表
    cardList(params: any) {
        return this._get('/api/apps-third-login/card-list', params)
    }

    // 文件上传接口
    upload(params: any) {
        return this._upload('/api/apps-third-login/upload', params)
    }

}


const loginService = new LoginService()

export default loginService