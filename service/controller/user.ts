import emailServer from "../util/emailServer";
import express from "express";
import utils, { readStaticFile } from "../util/utils";
import { addUser } from '../model/user';
// import jwtConfig from '../config/config.default';
// import jwt from "jsonwebtoken";
// import path from "path";

// 用户注册
export const register = async (req: express.Request, res: any, next: express.NextFunction) => {
    // 从请求体中 获取参数
    let { email } = req.body;
    try {
        let html = await readStaticFile(!utils.isDev ? '../../public/checkCode.html' : './checkCode.html');
        const passWord = new Date().getTime();
        html = html.replace('000000', email);
        html = html.replace('111111', passWord);
        await emailServer.sendMail({
            to: email, // 接受者邮箱,可以同时发送多个,以逗号隔开 
            subject: "托盘系统注册", // 邮件标题
            html
        });
        await addUser(email, passWord) as any;
        res.send({
            code: 200,
            message: '注册成功，用户名密码已成功发送至邮箱！'
        })
    } catch (error: any) {
        next({
            message: error.message
        })
    }

}

// 登录
export const login = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        let { user, ...ots } = req as any;
        let { password, ...ots2 } = user;

        console.log(1111111111);

        // 生成token
        // const token = await jwt.sign(
        //     {
        //         userId: user.id,
        //     },
        //     jwtConfig.jwtSecret,
        //     {
        //         expiresIn: 60 * 60 * 24,
        //     }
        // );

        // res.send({
        //     code: 200,
        //     message: '登录成功',
        //     data: {
        //         ...ots2,
        //         token
        //     }
        // })

        res.send({
            code: 200,
            message: '登录成功',
            data: 123123
        })
    } catch (error) {
    }
}
