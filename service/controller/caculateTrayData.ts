import emailServer from "../util/emailServer";
import express from "express";
import utils, { readStaticFile } from "../util/utils";
import { addTray } from '../model/trayDataList';
// import jwtConfig from '../config/config.default';
// import jwt from "jsonwebtoken";
// import path from "path";

// 新建
export const add = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        let { user } = req as any;
        let { bigObj } = req.body;
        let { id } = user;
        const resDt = await addTray(id, bigObj);
        console.log(resDt);

        res.send({
            code: 200,
            message: '新建成功',
            data: {
                // ...ots2,
            }
        })
    } catch (error) {

    }
}
