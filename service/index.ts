/*
 * @Author: xiaosihan 
 * @Date: 2022-05-08 14:25:26 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-09-29 09:45:12
 */

import cors from "cors";
import express from "express";
import dayjs from "dayjs";
import utils, { API } from "./util/utils";
import router from './router';
import morgan from 'morgan';
import errorHandler from './middleware/error-handler';


const app = express();

// app.use(cookieParser());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());


app.use(`${API}`, router);

// 在所有的中间件之后挂载错误处理中间件
app.use(errorHandler())

// 通常会在所有的中间件之后配置处理404内容
// 请求进来从上到下依次匹配
app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.status(404).send('很抱歉，没有找到请求地址');
})

console.log(process.env.NODE_ENV, 'pppppppppppppppppppppppp');


let env = "开发环境";
if (process.env.NODE_ENV === "test") {
    env = "测试环境";
} else if (process.env.NODE_ENV === "production") {
    env = "正式环境";
}

// 端口
const port = utils.isProd ? 8090 : 8080;

app.listen(port, () => console.log(env, "服务启动", port, dayjs().format("YYYY-MM-DD HH:mm:ss")));

// export {
//     client
// };
