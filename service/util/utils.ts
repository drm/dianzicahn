/*
 * @Author: xiaosihan 
 * @Date: 2022-07-27 02:55:02 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-07-27 02:56:16
 */
const fs = require("fs");
const { promisify } = require('util');
const path = require('path');

const utils = {

    isDev: process.env.NODE_ENV === "development",
    isTest: process.env.NODE_ENV === "test",
    isProd: process.env.NODE_ENV === "production",
    rand(start: number, end: number) {
        return Math.floor(Math.random() * (end - start + 1) + start);
    }
}

const readFile = promisify(fs.readFile);

export const readStaticFile = async (rootPath: string) => {
    const dbPath = path.join(__dirname, rootPath);
    const data = await readFile(dbPath, 'utf8')
    return data;
}

export const API = '/api';


export default utils;