import crypto from "crypto";

const md5 = (str: string) => {
    return crypto
        .createHash("md5")
        .update("cbd" + str) //加了一个混淆字符串，安全性更好 
        .digest("hex");
};

export default md5;
