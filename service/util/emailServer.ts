/*
 * @Author: xiaosihan 
 * @Date: 2022-06-08 10:41:22 
 * @Last Modified by: xiaosihan
 * @Last Modified time: 2022-06-08 10:50:07
 */

import nodemailer from "nodemailer";


interface MailOptions {
  to: string,
  subject: string,
  html: string
}

// 邮件服务
class EmailServer {

  constructor() {


  }

  transporter = nodemailer.createTransport({
    host: 'smtp.qq.com',
    secure: true,
    auth: {
      user: '389652405@qq.com',//输入你开启SMTP服务的QQ邮箱        
      pass: 'jleavvianeuzbidc'
    }
  });

  // mailOptions = {
  //   from: '389652405@qq.com', // 发送者,也就是你的QQ邮箱    
  //   to: '12938203121@qq.com', // 接受者邮箱,可以同时发送多个,以逗号隔开    
  //   subject: '测试发送邮件', // 邮件标题   
  //   html: `<p>这是我的测试邮件</p><p>哈哈哈，收到请回复</p>` //邮件内容，以html的形式输入，在邮件中会自动解析显示
  // };
  sendMail(mailOptions: MailOptions) {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail({ from: '389652405@qq.com', ...mailOptions }, function (err, data) {
        //回调函数，用于判断邮件是否发送成功  ...
        console.log("err", err);
        console.log("data", data);
        if (err) {
          reject(err)
        }
        resolve(data);
      });
    })

  }

}

const emailServer = new EmailServer();

export default emailServer;