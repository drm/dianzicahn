// 路由模块
import express from "express";
import database from "../database";
import { readStaticFile } from '../util/utils';
import { login, register } from '../controller/user';
import { validatoRegister, validatoLogin } from "../validator/user";
import { authValidate } from "../middleware/auth";

// 1.创建路由实例
// 路由实例其实就相当于下一个 mini Express 实例
const router = express.Router()

//新建
router.post('/add', register);

// 登录
router.post('/login', login);




//TOOD 获取用户信息
// app.post('/getUserInfo', user.getUserInfo);

export default router;
