import express from "express";
import caculateTrayData from "./caculateTrayData";
import user from './user';
import { authValidate } from "../middleware/auth";


const router = express.Router();

// 用户相关路由
router.use('/user', user);

// 编辑托盘信息
router.use('/caculateTrayData', authValidate, caculateTrayData);
export default router;