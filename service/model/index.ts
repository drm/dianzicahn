/*
 * @Author: xiaosihan 
 * @Date: 2022-06-01 10:21:29 
 * @Last Modified by: xiaosihan
 */

//@ts-ignore
import mysql from "mysql2";
import utils from "../util/utils";


const pool = mysql.createPool({
    host: utils.isProd ? "localhost" : '47.108.147.37',
    database: utils.isProd ? 'tray.prod' : 'tray.test',
    user: utils.isProd ? 'tray.prod' : 'tray.test',
    password: utils.isProd ? 'S3j4TzZZKtZMMPew' : 'scCdk8zNKaSRPLJa'
});

export const query = function (sql: string, values: (string | number)[]) {
    return new Promise((resolve, reject) => {
        pool.getConnection(function (err: any, conn: { query: (arg0: string, arg1: (string | number)[], arg2: (err: any, result: any) => void) => void; release: () => void; }) {
            if (err) {
                reject(err)
            } else {
                conn.query(sql, values, (err: any, result: unknown) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(result)
                    }

                    conn.release()
                })
            }
        })
    })
}

/**
 * 数据库对象
 *
 * @class Database
 */
// class Database {

//     // 添加用户


//     //   更新用户 token 通过用户名
//     async updataToken(id: string, token: number) {
//         await new Promise((resolve, reject) => {
//             this.connection.query(`UPDATE user token='${token}' WHERE id='${id}'`, function (error: any, results: any, fields: any) {
//                 if (error) {
//                     reject(error);
//                 }
//                 resolve(results[0]);
//             });
//         });

//     }

// }

// const database = new Database();

// export default database;

