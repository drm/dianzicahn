import SnowflakeID from "../util/uuid";
import md5 from "../util/md5";
import { query } from "./index";


// 判断是否登录成功
export const checkUserNamePass = async (email: string, password: string) => {

    let sql = "SELECT * FROM user WHERE email=? AND password=?";

    let params = [email, password];

    return await query(sql, params) // async 函数中return的依然是一个promise对象
}

// 查询邮箱用户名是否存在
export const checkEmailExit = async (email: string) => {
    let sql = "SELECT * FROM user WHERE email=?";

    let params = [email];
    return await query(sql, params);
}

// 添加用户
export const addUser = async (email: string, password: number) => {
    const snid = new SnowflakeID({
        mid: +new Date()
    });
    let id = snid.generate() as any;
    let sql = "INSERT INTO user (email, password, id, vip, disable) VALUES (?, ?, ?, '999', 1)";
    let params: (string | number)[] = [email, md5(String(password)), id];
    return await query(sql, params);
}

// 查询id用户是否存在
export const checkUserById = async (id: number) => {
    let sql = "SELECT * FROM user WHERE id=?";
    let params = [id];
    return await query(sql, params);
}

