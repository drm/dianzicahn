import { query } from "./index";
import dayjs from "dayjs";
import utils from "../util/utils";

type Iprop = {
    id: string,
    name: string, // 名字
    customerName: string, // 客户名称
    load: number, // 载重
    width: number, // 宽
    innerWidth: number, // 内装物宽度
    length: number, // 长
    innerLength: number, // 内装物长度
    height: number,
    innerHeight: number,
    child: string, // 每层托盘的数据
}

// 添加单个托盘数据
export const addTray = async (userId: string, bigObj: Iprop) => {

    let sql = "INSERT INTO trayProject (id, userId, creat_time, name, width, length, child) VALUES (?, ?, ?, ?, ?, ?, ?)";
    const id = dayjs().format('YY') + dayjs().format('MMDD') + (new Date().getTime() + '').slice(-6) + userId.slice(-5) + utils.rand(10, 99);
    const { name, length, width, child } = bigObj;
    let params: (string | number)[] = [id, userId, dayjs(`${new Date()}`).format('YYYY-MM-DD HH:mm:ss'), name, width, length, child];
    return await query(sql, params);
}

// 查询托盘id是否存在
export const checkTrayById = async (id: number) => {
    let sql = "SELECT * FROM trayProject WHERE id=?";
    let params = [id];
    return await query(sql, params);
}

// 查询用户下所有托盘数据
export const queryTrayAllData = async (userId: string) => {
    let sql = "SELECT * FROM trayProject WHERE userId=?";
    let params = [userId];
    return await query(sql, params);
}

// 更新托盘基础信息
export const updateTrayUserData = async (bigObj: Iprop) => {
    let sql = `update trayProject set name = ?, customerName = ?, load = ?, width = ?, innerWidth = ?, height = ?, innerHeight = ?, length = ?, innerLength = ? where id = ?`;
    const { name, customerName, load, width, innerWidth, height, innerHeight, length, innerLength, id } = bigObj;
    let params = [name, customerName, load, width, innerWidth, height, innerHeight, length, innerLength, id];
    return await query(sql, params);
}

// 更新托盘threejs大对象
export const updateTrayBigStringData = async (bigObj: Iprop) => {
    let sql = `update trayProject set child = ? where id = ?`;
    const { child, id } = bigObj;
    let params = [child, id];
    return await query(sql, params);
}
