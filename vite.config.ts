import react from '@vitejs/plugin-react'
import vitePluginImp from 'vite-plugin-imp'
import { fileURLToPath, URL } from 'url';
import { VitePWA } from 'vite-plugin-pwa';
import dayjs from "dayjs";

// https://vitejs.dev/config/
export default ({ mode }) => {

    const isDev = (mode === "development");
    const isProd = (mode === "production");

    const nowDate = dayjs().format("YYYY-MM-DD hh:mm:ss");

    return ({
        base: "./",
        // 全局替换
        define: {
            "process.env.BUILD_TIME": `'${nowDate}'`,
        },

        build: {
            assetsInlineLimit: 1024 * 10,
            minify: 'terser',
            terserOptions: {
                compress: {
                    drop_console: false,
                    drop_debugger: true,
                }
            },
            chunkSizeWarningLimit: 1024 * 100
        },
        plugins: [
            react(),
            false && vitePluginImp({
                optimize: true,
                libList: [
                    {
                        libName: 'antd',
                        libDirectory: 'es',
                        style: (name) => `antd/es/${name}/style`
                    },
                    {
                        libName: 'antd-mobile',
                        style: () => false,
                        libDirectory: 'es/components',
                        replaceOldImport: true
                    }
                ]
            }),
            false && VitePWA({
                scope: "/vite_react_mobx_threebase/",
                mode,
                minify: false,
                registerType: 'autoUpdate',
                workbox: {
                    // 自定义前端缓存的文件
                    globPatterns: ["**\/*.{js,css,html,png,jpg,jpeg,gif,ico,svg,wasm,glb,gltf,fbx,pdf,mp3,mp4,ttf,zip}"],
                    maximumFileSizeToCacheInBytes: 1024 * 1024 * 50, // 最大50MB
                }
            })
        ],
        resolve: {
            alias: {
                "@hashHistory": fileURLToPath(new URL('./src/hashHistory', import.meta.url)),
                "@utils": fileURLToPath(new URL('./src/utils/utils.ts', import.meta.url)),
                "@views": fileURLToPath(new URL('./src/views', import.meta.url)),
                "@globalStore": fileURLToPath(new URL('./src/globalStore.ts', import.meta.url)),
                "@components": fileURLToPath(new URL('./src/components', import.meta.url)),
                "@service": fileURLToPath(new URL('./src/service', import.meta.url)),
            }
        },
        css: {
            preprocessorOptions: {
                less: {
                    javascriptEnabled: true,// 支持内联 javascript
                },
            }
        },
        server: {
            proxy: {
                '/api': {
                    target: 'http://jsonplaceholder.typicode.com',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, '')
                },
                '/newmapserver': {
                    target: 'http://58.23.173.74:8703',
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/newmapserver/, '')
                },
            }
        }
    })

}
