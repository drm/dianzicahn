/*
 * @Author: xiaosihan 
 * @Date: 2022-04-23 22:42:15 
 * @Last Modified by: xiaosihan
 */
import { VitePluginNode } from 'vite-plugin-node';
import dayjs from "dayjs";

export default ({ mode }) => {
    const nowDate = dayjs().format("YYYY-MM-DD hh:mm:ss");

    return ({
        mode,
        // 全局替换
        define: {
            "process.env.NODE_ENV": `"production"`,
            "process.env.BUILD_TIME": `'${nowDate}'`,
        },

        build: {
            emptyOutDir: false,
            assetsInlineLimit: 1024 * 10,
            minify: 'terser',
            terserOptions: {
                compress: {
                    drop_console: false,
                    drop_debugger: true,
                }
            },
            chunkSizeWarningLimit: 1024 * 100
        },

        plugins: [
            ...VitePluginNode({
                adapter: 'express',
                appPath: './service/index.ts',
                exportName: 'viteNodeApp',
                // tsCompiler: 'swc'
            })
        ],

        ssr: {
            format: "cjs",
            // noExternal: ["cors", "express", "body-parser", "cookie-parser"],
            // noExternal: ["express", "qs", "nodemailer", "cors", "body-parser", "cookie-parser", "nodemailer", "stream", "tls", "events", "jsonwebtoken", "util"],
            target: "node"
        },

        resolve: {
            alias: []
        },

    });

}
